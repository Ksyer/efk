from fluent import sender
from fluent import event

# sender.setup('fluentd.test', host='localhost', port=24224)
sender.setup('fluentd.ksy2', host='localhost', port=24225)

event.Event('follow', {
  'from': 'A',
  'to':   'B',
})
