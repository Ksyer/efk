#!/usr/bin python
# -*- coding: utf-8 -*-

import os
import msgpack
import logging
import logging.config
from io import BytesIO
from fluent import handler
from pprint import pprint

def overflow_handler(pendings): # elasticsearch接受失败，将日志写入缓存中
    unpacker = msgpack.Unpacker(BytesIO(pendings))
    for unpacked in unpacker:
        print(unpacked)


def log(name):
    """
    return qogir_log module
    param: zabbix or api
    type: str
    """

    #创建日志文件夹
    basic_path = './1/'
    if not os.path.isdir(basic_path):
        os.mkdir(basic_path)

    #设置日志格式
    format = "%(asctime)-10s [%(levelname)s] %(module)s.%(funcName)s:%(lineno)d [ins-%(ins)s] %(message)s"

    proc = name.split("-")[0]

    files = {
        'alarm': basic_path + 'plugin-alarm.log',
        'api': basic_path + 'api.log',
        "fluentd.ksy2": basic_path + 'plugin-fluentd.log',
        'metric': basic_path + 'plugin-metric.log',
        'event': basic_path + 'plugin-event.log',
        'cluster': basic_path + 'plugin-cluster.log',
        'cloud_conf': basic_path + 'plugin-cloud_conf.log',
        'proxy': basic_path + 'plugin-proxy.log',
        'slb': basic_path + 'plugin-slb.log'
    }

    custom_format = {
        'host': '%(hostname)s', # 主机名，可能没有
        'where': '%(module)s.%(funcName)s', # 调用日志的模块名加函数名
        'type': '%(levelname)s', # 文本形式的日志级别
        'lineno': '%(lineno)s', # 调用日志输出函数的语句所在的代码行
        'filename': '%(filename)s', # 调用日志输出函数的模块的文件名
        'pathname': '%(pathname)s', # 调用日志输出函数的模块的完整路径名，可能没有
        'relativeCreated': '%(relativeCreated)d', # 输出日志信息时的，自Logger创建以 来的毫秒数
        'stack_trace': '%(exc_text)s'
    }

    formatter = logging.Formatter(format) # 设置日志格式
    file_hand = logging.FileHandler(files[proc]) # 设置日志路径
    file_hand.setLevel(logging.INFO) # 设置日志级别
    file_hand.setFormatter(formatter) # 设置日志格式

    log = logging.getLogger(name) # log_name 日志名称
    log.setLevel(logging.INFO)
    log.addHandler(file_hand)

    logging.basicConfig(level=logging.INFO) #设置fluent日志级别
    h = handler.FluentHandler(name, host='192.168.50.181', port=24224, buffer_overflow_handler=overflow_handler) #连接fluent
    print(h)
    formatter = handler.FluentRecordFormatter(custom_format)
    h.setFormatter(formatter)
    log.addHandler(h)

    return log


if __name__ == '__main__':
    log = log("fluentd.ksy2")
    ins = '804f8a914993a43ccaf7f2a48db40dd0f'
    log.info("This is a info message", extra={'ins': ins})
    log.critical("This is a critical message", extra={'ins': ins})
    log.info(
        {
            'logstash': '21',
            'item': 'item-xxxx',
            'TTLB': '0.123',
            'time_from': '2017-11-09 09:09:01'
        }, extra={'ins': ins}
    )