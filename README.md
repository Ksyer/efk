调用关系：
    logging -> Fluent -> ElasticSearch -> Kibana

logging 推送给 fluentd:

  对 logging 进行配置

fluentd 转发给 ES:

  配置 td-agent.conf

ES 的内容由 KB 负责呈现:
    
  保证版本号一样就行

```
<source>
  @type forward
  port 24226
</source>
<match fluentd.ksy2.**>
  @type elasticsearch
  flush_interval 1s
  host localhost
  port 9200
  include_tag_key true
  tag_key  @log_name
  logstash_format true
</match>
```

其中：
1. 2422x 是 fluentd 配置时的端口
2. 9200 / 9300 是 ES 的端口。 ES 服务端的端口一直是 9200 / 9300，客户端端口可以由 docker-compose 指定
3. 8889 / 8888 是 fluentd 对 http 的端口，用来验证本机和 Fluentd 是否联通
